package com.futurama;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
class FuturamaQuotesClient implements CommandLineRunner {

    private static final String RANDOM_QUOTES_SERVICE = "http://futurama-service/quotes/random";

    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${client.sleep.ms:3000}")
    private int sleepTime;

    @Override
    public void run(String... strings) throws Exception {
        System.out.println("Available instances:");
        discoveryClient.getInstances("futurama-service").forEach(s -> System.out.println(s));

        while(true) {
            try {
                Quote randomQuote = restTemplate.getForObject(RANDOM_QUOTES_SERVICE, Quote.class);
                System.out.println("[" + randomQuote.getSource() + "] " + randomQuote.getValue());

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            Thread.sleep(sleepTime);
        }
    }
}
