package com.futurama;

import java.io.InputStreamReader;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@RestController
@RequestMapping("quotes")
public class QuotesController {

    @Value("${spring.cloud.consul.discovery.instanceId}")
    private String instanceId;

    private List<Quote> quotes;

    @PostConstruct
    private List<Quote> quotes() {
        Gson parser = new Gson();
        ClassLoader classLoader = getClass().getClassLoader();
        quotes = parser.fromJson(new InputStreamReader(classLoader.getResourceAsStream("quotes.txt")), new TypeToken<List<Quote>>() {}.getType());
        return quotes;
    }

    @RequestMapping("/random")
    public Quote getRandomQuote() {
        Random rand = new Random();
        Quote quote = quotes.get(rand.nextInt(quotes.size() - 1));
        quote.setSource(instanceId);

        return quote;
    }

    @RequestMapping("/all")
    public List<Quote> getQuotes() {
        return quotes;
    }
}
